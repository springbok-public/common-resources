# common-resources

Useful common resources.  

## Chatbots (RASA)

After much consideration, the [PolyAI](https://www.polyai.com/) team has decided to take down the ConveRT models from the public domain.

We host a copy of the ConveRT model, so we can use it with our Rasa chatbots.  

We downloaded this from:  [Connor Brinton's](https://github.com/connorbrinton/polyai-models/releases/tag/v1.0) page

To use this model, retrain the models, with the new model_url configuration parameter for ConveRTTokenizer (and maybe ConveRTFeaturizer) set to the URL of the new [model](https://gitlab.com/springbok-public/common-resources/-/blob/master/model.tar.gz) location.  

For example, in `config.yml`
```
  - name: ConveRTTokenizer
    model_url: https://gitlab.com/springbok-public/common-resources/-/raw/master/model.tar.gz
```

### ConveRT Version 1

Original license:

```
Copyright 2020 PolyAI

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

Re-release license:

```
Copyright 2020 Connor Brinton

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```